<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

# auth
Auth::routes(['register' => false]);

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/calendar', 'CalendarController@index')->name('calendar');

Route::resource('/user', 'UserController')->except(['update', 'destroy']);

Route::resource('/article', 'ArticleController')->except(['update', 'destroy']);
Route::post('/article/{id}/update', 'ArticleController@update')->name('article.update');

Route::resource('service', 'ServiceController')->except(['update', 'destroy']);
Route::post('/service/{id}/update', 'ServiceController@update')->name('service.update');

Route::resource('/city_development', 'CityDevelopmentController')->except(['update', 'destroy']);
Route::post('/city_development/{id}/update', 'CityDevelopmentController@update')->name('city_development.update');

Route::get('/retrieve_list/article', 'ArticleController@retrieveList')->name('article.retrieve_list');
Route::get('/retrieve_list/city_development', 'CityDevelopmentController@retrieveList')->name('city_development.retrieve_list');
Route::get('/retrieve_list/service', 'ServiceController@retrieveList')->name('service.retrieve_list');
Route::get('/retrieve_list/user', 'UserController@retrieveList')->name('user.retrieve_list');
Route::get('/retrieve_list/calendar/', 'CalendarController@retrieveDateList')->name('calendar.retrieve_dates');