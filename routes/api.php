<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/user/login', 'API\v1\LoginController@login');
Route::post('/user/signup', 'API\v1\SignupController@signup');

Route::get('/article/headline', 'API\v1\ArticleController@getArticleHeadline');
Route::get('/articles', 'API\v1\ArticleController@getInitialArticles');
Route::get('/articles/newer/{article_id}', 'API\v1\ArticleController@getNewerArticles');
Route::get('/articles/older/{article_id}', 'ApI\v1\ArticleController@getOlderArticles');

Route::get('/articles/popular', 'API\v1\ArticleController@getInitialPopularArticles');
Route::get('/articles/popular/newer/{article_id}', 'API\v1\ArticleController@getNewerPopularArticles');
Route::get('/articles/popular/older/{article_id}', 'ApI\v1\ArticleController@getOlderPopularArticles');

Route::post('/posts', 'API\v1\PostController@createPost');
Route::get('/posts', 'API\v1\PostController@getInitialPosts');
Route::get('/posts/newer/{post_id}', 'API\v1\PostController@getNewerPosts');
Route::get('/posts/older/{post_id}', 'API\v1\PostController@getOlderPosts');

Route::get('/services', 'API\v1\ServiceController@getServices');

Route::get('/user/profile', 'API\v1\UserProfileController@getUserProfile');
Route::get('/user/permits', 'API\v1\UserPermitController@getUserPermits');
Route::post('/user/booking', 'API\v1\UserBookingController@bookSchedule');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
