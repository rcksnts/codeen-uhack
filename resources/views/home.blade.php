@extends('layouts.app')

@section('header_left')
    <h6 class="h2 text-white d-inline-block mb-0">Dashboard</h6>
    <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
        <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
            <li class="breadcrumb-item"><a href="/home"><i class="fas fa-home"></i></a></li>
            <li class="breadcrumb-item"><a href="/home">Dashboard </a></li>
        </ol>
    </nav>
@endsection

@section('content')
    <div class="row">
        <div class="col-xl-3 col-md-6">
            <div class="card card-stats">
                <!-- Card body -->
                <div class="card-body">
                    <div class="row">
                        <div class="col">
                            <h5 class="card-title text-uppercase text-muted mb-0">New users</h5>
                            <span class="h2 font-weight-bold mb-0">2,356</span>
                        </div>
                        <div class="col-auto">
                            <div class="icon icon-shape bg-gradient-orange text-white rounded-circle shadow">
                                <i class="ni ni-user-run"></i>
                            </div>
                        </div>
                    </div>
                    {{--                    <p class="mt-3 mb-0 text-sm">--}}
                    {{--                        <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> xx.xx%</span>--}}
                    {{--                        <span class="text-nowrap">Since last month</span>--}}
                    {{--                    </p>--}}
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-md-6">
            <div class="card card-stats">
                <!-- Card body -->
                <div class="card-body">
                    <div class="row">
                        <div class="col">
                            <h5 class="card-title text-uppercase text-muted mb-0">Registered services</h5>
                            <span class="h2 font-weight-bold mb-0">59</span>
                        </div>
                        <div class="col-auto">
                            <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
                                <i class="ni ni-active-40"></i>
                            </div>
                        </div>
                    </div>
                    {{--                    <p class="mt-3 mb-0 text-sm">--}}
                    {{--                        <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> 3.48%</span>--}}
                    {{--                        <span class="text-nowrap">Since last month</span>--}}
                    {{--                    </p>--}}
                </div>
            </div>
        </div>

        <div class="col-xl-3 col-md-6">
            <div class="card card-stats">
                <!-- Card body -->
                <div class="card-body">
                    <div class="row">
                        <div class="col">
                            <h5 class="card-title text-uppercase text-muted mb-0">Posted Articles</h5>
                            <span class="h2 font-weight-bold mb-0">42</span>
                        </div>
                        <div class="col-auto">
                            <div class="icon icon-shape bg-gradient-green text-white rounded-circle shadow">
                                <i class="ni ni-archive-2"></i>
                            </div>
                        </div>
                    </div>
                    {{--                    <p class="mt-3 mb-0 text-sm">--}}
                    {{--                        <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> 3.48%</span>--}}
                    {{--                        <span class="text-nowrap">Since last month</span>--}}
                    {{--                    </p>--}}
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-md-6">
            <div class="card card-stats">
                <!-- Card body -->
                <div class="card-body">
                    <div class="row">
                        <div class="col">
                            <h5 class="card-title text-uppercase text-muted mb-0">City Developments</h5>
                            <span class="h2 font-weight-bold mb-0">10</span>
                        </div>
                        <div class="col-auto">
                            <div class="icon icon-shape bg-gradient-info text-white rounded-circle shadow">
                                <i class="ni ni-chart-bar-32"></i>
                            </div>
                        </div>
                    </div>
                    {{--                    <p class="mt-3 mb-0 text-sm">--}}
                    {{--                        <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> 3.48%</span>--}}
                    {{--                        <span class="text-nowrap">Since last month</span>--}}
                    {{--                    </p>--}}
                </div>
            </div>
        </div>

        <div class="col-xl-4">
            <!-- Progress track -->
            <div class="card">
                <!-- Card header -->
                <div class="card-header">
                    <!-- Title -->
                    <h5 class="h3 mb-0"> Track city development </h5>
                </div>
                <!-- Card body -->
                <div class="card-body">
                    <!-- List group -->
                    <ul class="list-group list-group-flush list my--3">
                        <li class="list-group-item px-0">
                            <div class="row align-items-center">
                                <div class="col-auto">
                                </div>
                                <div class="col">
                                    <h5>City Design System</h5>
                                    <div class="progress progress-xs mb-0">
                                        <div class="progress-bar bg-orange" role="progressbar" aria-valuenow="60"
                                             aria-valuemin="0" aria-valuemax="100" style="width: 60%;"></div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item px-0">
                            <div class="row align-items-center">
                                <div class="col-auto">
                                </div>
                                <div class="col">
                                    <h5>Black-market Now</h5>
                                    <div class="progress progress-xs mb-0">
                                        <div class="progress-bar bg-green" role="progressbar" aria-valuenow="100"
                                             aria-valuemin="0" aria-valuemax="100" style="width: 100%;"></div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item px-0">
                            <div class="row align-items-center">
                                <div class="col-auto">
                                </div>
                                <div class="col">
                                    <h5>Blackboard School Development Center</h5>
                                    <div class="progress progress-xs mb-0">
                                        <div class="progress-bar bg-red" role="progressbar" aria-valuenow="72"
                                             aria-valuemin="0" aria-valuemax="100" style="width: 72%;"></div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item px-0">
                            <div class="row align-items-center">
                                <div class="col-auto">
                                </div>
                                <div class="col">
                                    <h5>React Material Store</h5>
                                    <div class="progress progress-xs mb-0">
                                        <div class="progress-bar bg-teal" role="progressbar" aria-valuenow="90"
                                             aria-valuemin="0" aria-valuemax="100" style="width: 90%;"></div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endsection
