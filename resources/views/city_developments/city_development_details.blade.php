@extends('layouts.app')

@section('header_left')
    <h6 class="h2 text-white d-inline-block mb-0">{{ $model_object['model'] }}</h6>
    <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
        <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
            <li class="breadcrumb-item"><a href="/"><i class="fas fa-home"></i></a></li>
            <li class="breadcrumb-item"><a href="/home">Dashboard </a></li>
            @foreach ($model_object['breadcrumb'] as $key => $link)
                <li class="breadcrumb-item"><a href="{{ $link }}"> {{ $key }} </a></li>
            @endforeach
            <li class="breadcrumb-item active" aria-current="page">View</li>
        </ol>
    </nav>
@endsection

@section('header_right')
    <a href="{{ url($model_object['model_path']) }}" class="btn btn-sm btn-neutral text-default">
        <span class="btn-inner--icon"><i class="ni ni-bold-left"></i></span>
        Back to Data List
    </a>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card-wrapper">
                <div class="card">
                    <div class="card-header">
                        <h3 class="mb-0 font-weight-900">
                            View Existing {{ $model_object['model'] }}
                        </h3>
                    </div>

                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-control-label" for="form-name"> Name * </label>
                                    <input type="text"
                                           class="form-control @if ($errors->has('name')) is-invalid @endif"
                                           id="form-name" name="name"
                                           value="{{ $detail_collection->name ?? old('name') }}" disabled>
                                </div>

                                <label class="form-control-label" for="textarea-description">Description *</label>
                                <textarea class="form-control" id="textarea-description" name="description"
                                          rows="3" readonly>{{ $detail_collection->description }}</textarea>

                                <hr>

                                <div class="form-group">
                                    <label class="form-control-label" for="form-percentage"> Percentage of
                                        Completion * </label>
                                    <input type="number"
                                           class="form-control @if ($errors->has('percentage')) is-invalid @endif"
                                           id="form-percentage" name="percentage"
                                           value="{{ $detail_collection->percentage ?? old('percentage') }}" disabled>
                                </div>

                                <div class="form-group">
                                    <label class="form-control-label" for="form-stage">Current Stage *</label>
                                    <select class="form-control @if ($errors->has('stage')) is-invalid @endif"
                                            data-toggle="select" name="stage" disabled>
                                        <option value="CTDEV01"
                                                @if ($detail_collection->status == 'CTDEV01') selected @endif>
                                            Foundation and Concreting
                                        </option>
                                        <option value="CTDEV02"
                                                @if ($detail_collection->status == 'CTDEV02') selected @endif>
                                            Painting
                                        </option>
                                        <option value="CTDEV03"
                                                @if ($detail_collection->status == 'CTDEV03') selected @endif>
                                            Ceiling and Tiling
                                        </option>
                                        <option value="CTDEV04"
                                                @if ($detail_collection->status == 'CTDEV04') selected @endif>
                                            Inspection
                                        </option>
                                        <option value="CTDEV05"
                                                @if ($detail_collection->status == 'CTDEV05') selected @endif>
                                            Complete
                                        </option>
                                    </select>
                                </div>

                                <button class="btn btn-default" type="submit">Save Changes</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection