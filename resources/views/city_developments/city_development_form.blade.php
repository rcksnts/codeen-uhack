@extends('layouts.app')

@section('header_left')
    <h6 class="h2 text-white d-inline-block mb-0">{{ $model_object['model'] }}</h6>
    <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
        <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
            <li class="breadcrumb-item"><a href="/home"><i class="fas fa-home"></i></a></li>
            <li class="breadcrumb-item"><a href="/home">Dashboard </a></li>
            @foreach ($model_object['breadcrumb'] as $key => $link)
                <li class="breadcrumb-item"><a href="{{ $link }}"> {{ $key }} </a></li>
            @endforeach
        </ol>
    </nav>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card-wrapper">
                <div class="card">
                    <div class="card-header">
                        <h3 class="mb-0 font-weight-900">
                            @switch($model_object['action'])
                                @case('create')
                                Create New {{ $model_object['model'] }}
                                @break
                                @case('edit')
                                Update Existing {{ $model_object['model'] }}
                                @break
                                @default
                            @endswitch
                        </h3>
                    </div>

                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <form method="post" action="{{ $model_object['form_action'] }}" role="form"
                                      id="form-data">
                                    @csrf
                                    <div class="form-group">
                                        <label class="form-control-label" for="form-name"> Name * </label>
                                        <input type="text"
                                               class="form-control @if ($errors->has('name')) is-invalid @endif"
                                               id="form-name" name="name"
                                               value="{{ $detail_collection->name ?? old('name') }}">
                                        @if ($errors->has('name'))
                                            <div class="invalid-feedback d-block">
                                                {{ $errors->first('name') }}
                                            </div>
                                        @endif
                                    </div>

                                    <div class="form-group">
                                        <label class="form-control-label" for="form-description"> Description * </label>
                                        <textarea class="form-control" id="form-description" name="description"
                                                  rows="3">{{ $detail_collection->description ?? old('description') }}</textarea>
                                        @if ($errors->has('description'))
                                            <div class="invalid-feedback d-block">
                                                {{ $errors->first('description') }}
                                            </div>
                                        @endif
                                    </div>

                                    <hr>

                                    <div class="form-group">
                                        <label class="form-control-label" for="form-percentage"> Percentage of
                                            Completion * </label>
                                        <input type="number"
                                               class="form-control @if ($errors->has('percentage')) is-invalid @endif"
                                               id="form-percentage" name="percentage"
                                               value="{{ $detail_collection->percentage ?? old('percentage') }}"
                                               @if ($model_object['action'] == 'create') disabled @endif>
                                        @if ($errors->has('percentage'))
                                            <div class="invalid-feedback d-block">
                                                {{ $errors->first('percentage') }}
                                            </div>
                                        @endif
                                    </div>

                                    <div class="form-group">
                                        <label class="form-control-label" for="form-stage">Current Stage *</label>
                                        <select class="form-control @if ($errors->has('stage')) is-invalid @endif"
                                                data-toggle="select" name="stage"
                                                @if ($model_object['action'] == 'create') disabled @endif>
                                            <option value="CTDEV01"
                                                    @if (isset($detail_collection)) @if ($detail_collection->status == 'CTDEV01') selected @endif @endif>
                                                Foundation and Concreting
                                            </option>
                                            <option value="CTDEV02"
                                                    @if (isset($detail_collection)) @if ($detail_collection->status == 'CTDEV02') selected @endif @endif>
                                                Painting
                                            </option>
                                            <option value="CTDEV03"
                                                    @if (isset($detail_collection)) @if ($detail_collection->status == 'CTDEV03') selected @endif @endif>
                                                Ceiling and Tiling
                                            </option>
                                            <option value="CTDEV04"
                                                    @if (isset($detail_collection)) @if ($detail_collection->status == 'CTDEV04') selected @endif @endif>
                                                Inspection
                                            </option>
                                            <option value="CTDEV05"
                                                    @if (isset($detail_collection)) @if ($detail_collection->status == 'CTDEV05') selected @endif @endif>
                                                Complete
                                            </option>
                                        </select>
                                    </div>

                                    <button class="btn btn-default" type="submit">Save Changes</button>
                                </form>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection