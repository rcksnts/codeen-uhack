@extends('layouts.app')

@section('header_left')
    <h6 class="fullcalendar-title h2 text-white d-inline-block mb-0">Full calendar</h6>
    <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
        <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
            <li class="breadcrumb-item"><a href="/dashboard"><i class="fas fa-home"></i></a></li>
            <li class="breadcrumb-item"><a href="/dashboard">Dashboard </a></li>
            <li class="breadcrumb-item active" aria-current="page">Calendar</li>
        </ol>
    </nav>
@endsection

@section('header_right')
    <a href="#" class="fullcalendar-btn-prev btn btn-sm btn-neutral text-default">
        <i class="fas fa-angle-left"></i>
    </a>
    <a href="#" class="fullcalendar-btn-next btn btn-sm btn-neutral text-default">
        <i class="fas fa-angle-right"></i>
    </a>
    <a href="#" class="btn btn-sm btn-neutral text-default" data-calendar-view="month">Month</a>
    <a href="#" class="btn btn-sm btn-neutral text-default" data-calendar-view="basicWeek">Week</a>
    <a href="#" class="btn btn-sm btn-neutral text-default" data-calendar-view="basicDay">Day</a>
@endsection

@section('content')
    <div class="row">
        <div class="col">
            <div class="card card-calendar">
                <div class="card-header">
                    <h5 class="h3 mb-0 font-weight-bold">Calendar</h5>
                </div>
                <div class="card-body p-0">
                    <div class="calendar" data-toggle="calendar" id="calendar"></div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('styles')
    <link href="{{ asset('vendor/fullcalendar/dist/fullcalendar.min.css') }}" rel="stylesheet"
          type="text/css">
@endsection

@section('scripts')
    <script src="{{ asset('vendor/moment/min/moment.min.js') }}"></script>
    <script src="{{ asset('vendor/fullcalendar/dist/fullcalendar.min.js') }}"></script>
@endsection