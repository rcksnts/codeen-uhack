@extends('layouts.app')

@section('header_left')
    <h6 class="h2 text-white d-inline-block mb-0">{{ $model_object['model'] }}</h6>
    <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
        <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
            <li class="breadcrumb-item"><a href="/dashboard"><i class="fas fa-home"></i></a></li>
            <li class="breadcrumb-item"><a href="/dashboard">Dashboard </a></li>
            @foreach ($model_object['breadcrumb'] as $key => $link)
                <li class="breadcrumb-item"><a href="{{ $link }}"> {{ $key }} </a></li>
            @endforeach
        </ol>
    </nav>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="nav-wrapper">
                <ul class="nav nav-pills nav-fill flex-column flex-md-row" id="tabs-icons-text" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link mb-sm-3 mb-md-0 active font-weight-bold" id="tabs-user-details-tab"
                           data-toggle="tab" href="#tabs-user-details" role="tab" aria-controls="tabs-user-details"
                           aria-selected="true"><i class="fas fa-user"></i> &nbsp; User Details</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link mb-sm-3 mb-md-0 font-weight-bold" id="tabs-permit-tracking-tab"
                           data-toggle="tab"
                           href="#tabs-permit-tracking" role="tab" aria-controls="tabs-permit-tracking"
                           aria-selected="false">
                            <i class="far fa-folder-open"></i> &nbsp; Permit Tracking</a>
                    </li>
                </ul>
            </div>

            <div class="tab-content">
                <div class="tab-pane fade show active" id="tabs-user-details">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-header">
                                    <h3 class="mb-0 font-weight-900">
                                        User Details
                                    </h3>
                                </div>

                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-5">
                                            <p class="h5 text-muted">CUSTOMER INFORMATION</p>
                                            <dl class="row pb-2">
                                                <dt class="col-sm-6">
                                                    <p class="m-0 font-weight-bold">Email</p>
                                                </dt>
                                                <dd class="col-sm-6">
                                                    <p class="m-0">
                                                        {{ $detail_collection->email }}
                                                    </p>
                                                </dd>
                                                <dt class="col-sm-6">
                                                    <p class="m-0 font-weight-bold">First Name</p>
                                                </dt>
                                                <dd class="col-sm-6">
                                                    <p class="m-0">
                                                        {{ $detail_collection->user_detail->first_name }}
                                                    </p>
                                                </dd>
                                                <dt class="col-sm-6">
                                                    <p class="m-0 font-weight-bold">Last Name</p>
                                                </dt>
                                                <dd class="col-sm-6">
                                                    <p class="m-0">
                                                        {{ $detail_collection->user_detail->last_name }}
                                                    </p>
                                                </dd>
                                            </dl>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="tabs-permit-tracking">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-header">
                                    <div class="row align-items-center">
                                        <div class="col-8">
                                            <h5 class="h3 mb-0 font-weight-900">
                                                Permit Tracker
                                            </h5>
                                        </div>

                                        <div class="col-4 text-right">
                                            <button type="button" class="btn btn-sm btn-outline-default"
                                                    data-toggle="modal"
                                                    data-target="#modal-file-update-upload">
                                                <span class="btn-inner--icon"><i class="fas fa-file-upload"></i></span>
                                                &nbsp; Upload File
                                            </button>
                                        </div>
                                    </div>
                                </div>

                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <dl class="row">
                                                <dt class="col-sm-6">
                                                    <p class="m-0 font-weight-bold">Permit Type #1</p>
                                                    <p class="m-0 description">Last Updated : — </p>
                                                    <p class="m-0 description">Status: —</p>
                                                </dt>
                                                <dd class="col-sm-6 float-right">
                                                    <p class="m-0">
                                                        <span class="text-muted italic">— No recent update found —</span>
                                                    </p>
                                                </dd>
                                            </dl>
                                        </div>

                                        <div class="col-md-6">
                                            <dl class="row">
                                                <dt class="col-sm-6">
                                                    <p class="m-0 font-weight-bold">Permit Type #2</p>
                                                    <p class="m-0 description">Last Updated : — </p>
                                                    <p class="m-0 description">Status: —</p>
                                                </dt>
                                                <dd class="col-sm-6 float-right">
                                                    <p class="m-0">
                                                        <span class="text-muted italic">— No recent update found —</span>
                                                    </p>
                                                </dd>
                                            </dl>
                                        </div>

                                        <div class="col-md-6">
                                            <dl class="row">
                                                <dt class="col-sm-6">
                                                    <p class="m-0 font-weight-bold">Permit Type #3</p>
                                                    <p class="m-0 description">Last Updated : — </p>
                                                    <p class="m-0 description">Status: —</p>
                                                </dt>
                                                <dd class="col-sm-6 float-right">
                                                    <p class="m-0">
                                                        <span class="text-muted italic">— No recent update found —</span>
                                                    </p>
                                                </dd>
                                            </dl>
                                        </div>

                                        <div class="col-md-6">
                                            <dl class="row">
                                                <dt class="col-sm-6">
                                                    <p class="m-0 font-weight-bold">Permit Type #4</p>
                                                    <p class="m-0 description">Last Updated : — </p>
                                                    <p class="m-0 description">Status: —</p>
                                                </dt>
                                                <dd class="col-sm-6 float-right">
                                                    <p class="m-0">
                                                        <span class="text-muted italic">— No recent update found —</span>
                                                    </p>
                                                </dd>
                                            </dl>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

{{--@section('styles')--}}
{{--    <link href="{{ asset('vendor/datatables.net-bs4/css/dataTables.bootstrap4.min.css') }}" rel="stylesheet"--}}
{{--          type="text/css">--}}
{{--    <link href="{{ asset('vendor/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css') }}" rel="stylesheet"--}}
{{--          type="text/css">--}}
{{--    <link href="{{ asset('vendor/datatables.net-select-bs4/css/select.bootstrap4.min.css') }}" rel="stylesheet"--}}
{{--          type="text/css">--}}
{{--@endsection--}}

{{--@section('scripts')--}}
{{--    <script src="{{ asset('vendor/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>--}}
{{--    <script src="{{ asset('vendor/datatables.net/js/jquery.dataTables.min.js') }}"></script>--}}
{{--    <script src="{{ asset('vendor/datatables.net-bs4/js/dataTables.bootstrap4.min.js') }}"></script>--}}
{{--    <script src="{{ asset('vendor/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>--}}
{{--    <script src="{{ asset('vendor/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js') }}"></script>--}}
{{--    <script src="{{ asset('vendor/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>--}}
{{--    <script src="{{ asset('vendor/datatables.net-buttons/js/buttons.flash.min.js') }}"></script>--}}
{{--    <script src="{{ asset('vendor/datatables.net-buttons/js/buttons.print.min.js') }}"></script>--}}
{{--    <script src="{{ asset('vendor/datatables.net-select/js/dataTables.select.min.js') }}"></script>--}}
{{--@endsection--}}