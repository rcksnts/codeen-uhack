@extends('layouts.app')

@section('header_left')
    <h6 class="h2 text-white d-inline-block mb-0">{{ $model_object['model'] }}</h6>
    <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
        <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
            <li class="breadcrumb-item"><a href="/home"><i class="fas fa-home"></i></a></li>
            <li class="breadcrumb-item"><a href="/home">Dashboard </a></li>
            @foreach ($model_object['breadcrumb'] as $key => $link)
                <li class="breadcrumb-item"><a href="{{ $link }}"> {{ $key }} </a></li>
            @endforeach
        </ol>
    </nav>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card-wrapper">
                <div class="card">
                    <div class="card-header">
                        <h3 class="mb-0 font-weight-900">
                            View Existing {{ $model_object['model'] }}
                        </h3>
                    </div>

                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <img class="img-fluid" src="{{ url($detail_collection->image) }}">
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-control-label" for="form-name"> Name * </label>
                                    <input type="text"
                                           class="form-control @if ($errors->has('name')) is-invalid @endif"
                                           id="form-name" name="name" disabled
                                           value="{{ $detail_collection->name ?? old('name') }}">
                                </div>

                                <label class="form-control-label" for="textarea-description">Description *</label>
                                <textarea class="form-control" id="textarea-description" name="description"
                                          rows="3" readonly>{{ $detail_collection->description }}</textarea>

                                <div class="form-group">
                                    <label class="form-control-label" for="form-name"> Service Type * </label>
                                    <input type="text"
                                           class="form-control"
                                           id="form-name" name="name" disabled
                                           value="{{ $detail_collection->service_type ?? old('service_type') }}">
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection