@extends('layouts.app')

@section('header_left')
    <h6 class="h2 text-white d-inline-block mb-0">{{ $model_object['model'] }}</h6>
    <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
        <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
            <li class="breadcrumb-item"><a href="/home"><i class="fas fa-home"></i></a></li>
            <li class="breadcrumb-item"><a href="/home">Dashboard </a></li>
            @foreach ($model_object['breadcrumb'] as $key => $link)
                <li class="breadcrumb-item"><a href="{{ $link }}"> {{ $key }} </a></li>
            @endforeach
        </ol>
    </nav>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card-wrapper">
                <div class="card">
                    <div class="card-header">
                        <h3 class="mb-0 font-weight-900">
                            @switch($model_object['action'])
                                @case('create')
                                Create New {{ $model_object['model'] }}
                                @break
                                @case('edit')
                                Update Existing {{ $model_object['model'] }}
                                @break
                                @default
                            @endswitch
                        </h3>
                    </div>

                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <form method="post" action="{{ $model_object['form_action'] }}" role="form"
                                      id="form-data" enctype="multipart/form-data">
                                    @csrf
                                    @if ($model_object['action'] == 'create')
                                        <div class="form-group">
                                            <div class="main-img-preview">
                                                <img class="thumbnail img-preview"
                                                     title="Photo Preview">
                                            </div>
                                            <br>
                                            <div class="input-group">
                                                <input id="unitUpdateUpload" class="form-control fake-shadow"
                                                       placeholder="Choose File"
                                                       disabled="disabled">
                                                <div class="input-group-append">
                                                    <div class="fileUpload btn btn-default fake-shadow">
                                                        <span><i class="fas fa-upload"></i> Upload Photo</span>
                                                        <input id="photo-id" name="image" type="file"
                                                               class="attachment_upload"
                                                               accept="image/*" value="{{ old('image') }}">
                                                    </div>
                                                </div>
                                            </div>
                                            @if ($errors->has('image'))
                                                <div class="invalid-feedback d-block">
                                                    {{ $errors->first('image') }}
                                                </div>
                                            @endif
                                        </div>
                                    @else
                                        <div class="col-md-6 pb-4">
                                            <img class="img-fluid" src="{{ url($detail_collection->image) }}">
                                        </div>
                                    @endif

                                    <div class="form-group">
                                        <label class="form-control-label" for="form-name"> Name * </label>
                                        <input type="text"
                                               class="form-control @if ($errors->has('name')) is-invalid @endif"
                                               id="form-name" name="name"
                                               value="{{ $detail_collection->name ?? old('name') }}">
                                        @if ($errors->has('name'))
                                            <div class="invalid-feedback d-block">
                                                {{ $errors->first('name') }}
                                            </div>
                                        @endif
                                    </div>

                                    <div class="form-group">
                                        <label class="form-control-label" for="form-description"> Description * </label>
                                        <textarea class="form-control" id="form-description" name="description"
                                                  rows="3">{{ $detail_collection->description ?? old('description') }}</textarea>
                                        @if ($errors->has('description'))
                                            <div class="invalid-feedback d-block">
                                                {{ $errors->first('description') }}
                                            </div>
                                        @endif
                                    </div>

                                    <div class="form-group">
                                        <label class="form-control-label" for="form-service_type">Service Type *</label>
                                        <select class="form-control @if ($errors->has('service_type')) is-invalid @endif"
                                                data-toggle="select" name="service_type">
                                            <option value=""> — Select Service Type —</option>
                                            @foreach ($service_types as $service_type)
                                                <option value="{{ $service_type->id }}"
                                                        @if (isset($detail_collection))
                                                        @if ($service_type->id == $detail_collection->service_type_id)
                                                        selected
                                                        @endif
                                                        @endif>
                                                    {{ $service_type->name }}
                                                </option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('service_type'))
                                            <div class="invalid-feedback d-block">
                                                {{ $errors->first('service_type') }}
                                            </div>
                                        @endif
                                    </div>

                                    <button class="btn btn-default" type="submit">Save Changes</button>
                                </form>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection

@section('styles')
    <style>
        .fake-shadow {
            box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
        }

        .fileUpload {
            position: relative;
            overflow: hidden;
        }

        .fileUpload #photo-id {
            position: absolute;
            top: 0;
            right: 0;
            margin: 0;
            padding: 0;
            font-size: 33px;
            cursor: pointer;
            opacity: 0;
            filter: alpha(opacity=0);
        }

        .img-preview {
            max-width: 100%;
        }
    </style>
@endsection

@section('scripts')
    <script src="{{ asset('vendor/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            var brand = document.getElementById('photo-id');
            brand.className = 'attachment_upload';
            brand.onchange = function () {
                document.getElementById('unitUpdateUpload').value = this.value.substring(12);
            };

            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('.img-preview').attr('src', e.target.result);
                    };
                    reader.readAsDataURL(input.files[0]);
                }
            }

            $("#photo-id").change(function () {
                readURL(this);
            });
        });
    </script>
@endsection