@extends('layouts.app')

@section('header_left')
    <h6 class="h2 text-white d-inline-block mb-0">{{ $model_object['model'] }}</h6>
    <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
        <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
            <li class="breadcrumb-item"><a href="/"><i class="fas fa-home"></i></a></li>
            <li class="breadcrumb-item"><a href="/home">Dashboard </a></li>
        </ol>
    </nav>
@endsection

@section('header_right')
    <a href="{{ url($model_object['form_action']) }}" class="btn btn-sm btn-neutral text-default">
        <span class="btn-inner--icon"><i class="ni ni-fat-add"></i></span>
        Create New
    </a>
@endsection

@section('content')
    <div class="row">
        <div class="col">
            <div class="card-wrapper">
                <div class="card">
                    <div class="card-header">
                        <h3 class="mb-0 font-weight-900">{{ $model_object['model'] }} Data List</h3>
                        @if(session()->has('success'))
                            <br>
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                <span class="alert-icon"><i class="ni ni-like-2"></i></span>
                                <span class="alert-text"><strong>Success!</strong> {{ session()->get('success') }} </span>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif
                        @if(session()->has('error'))
                            <br>
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                <span class="alert-icon"><i class="fas fa-exclamation-circle"></i></span>
                                <span class="alert-text"><strong>Error!</strong> {{ session()->get('error') }} </span>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif
                        @if(session()->has('warning'))
                            <br>
                            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                                <span class="alert-icon"><i class="fas fa-info-circle"></i></span>
                                <span class="alert-text"><strong>Warning!</strong> {{ session()->get('warning') }} </span>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif
                    </div>
                    <div class="table-responsive py-4">
                        <table class="table table-flush table-hover" id="datatable-list" width="100%">
                            <thead class="thead-light">
                            <tr>
                                @foreach ($model_object['header_list'] as $header)
                                    <th> {{ strtoupper($header) }} </th>
                                @endforeach
                                <th width="15%"> UPDATED AT</th>
                                <th width="12%"> ACTION</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                @foreach ($model_object['header_list'] as $header)
                                    <th> {{ ucwords($header) }} </th>
                                @endforeach
                                <th width="15%"> Updated At</th>
                                <th width="12%"> Action</th>
                            </tr>
                            </tfoot>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('styles')
    <link href="{{ asset('vendor/datatables.net-bs4/css/dataTables.bootstrap4.min.css') }}" rel="stylesheet"
          type="text/css">
    <link href="{{ asset('vendor/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css') }}" rel="stylesheet"
          type="text/css">
    <link href="{{ asset('vendor/datatables.net-select-bs4/css/select.bootstrap4.min.css') }}" rel="stylesheet"
          type="text/css">
    <style>
        table {
            table-layout: fixed;
        }

        td {
            word-break: break-word;
            white-space: normal !important;
        }
    </style>
@endsection

@section('scripts')
    <script src="{{ asset('vendor/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('vendor/datatables.net-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('vendor/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('vendor/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('vendor/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('vendor/datatables.net-buttons/js/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('vendor/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('vendor/datatables.net-select/js/dataTables.select.min.js') }}"></script>
    <script>
        var DatatableList = (function () {
            var $dtList = $('#datatable-list');

            function init($this) {
                var model = '{{ strtolower($model_object['model']) }}';
                var ajax_url = '/retrieve_list/' + model.split(' ').join('_');
                var buttons = ["copy", "csv"];
                var options = {
                    bDestroy: true,
                    bAutoWidth: true,
                    processing: true,
                    serverSide: true,
                    lengthChange: true,
                    pageLength: 10,
                    buttons: buttons,
                    language: {
                        paginate: {
                            previous: "<i class='fas fa-angle-left'>",
                            next: "<i class='fas fa-angle-right'>"
                        }
                    },
                    ajax: {
                        "url": ajax_url,
                        "dataType": "json",
                        "type": "GET",
                        "data": {_token: "{{ csrf_token() }}"}
                    },
                    @switch(strtolower($model_object['model']))
                            @case('article')
                    columns: [
                        {data: 'title', name: 'title'},
                        {data: 'body', name: 'body'},
                        {data: 'view_count', name: 'view_count'},
                        {data: 'updated_at', name: 'updated_at'},
                        {data: 'action_column', name: 'action_column', orderable: false, searchable: false},
                    ],
                    @break

                    @endswitch
                };

                // Init the datatable
                var table = $this.on('init.dt', function () {
                    $('div.dataTables_length select').removeClass('custom-select custom-select-sm');
                }).DataTable(options);
            }

            if ($dtList.length) {
                init($dtList);
            }
        })();
    </script>
@endsection