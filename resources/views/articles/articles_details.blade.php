@extends('layouts.app')

@section('header_left')
    <h6 class="h2 text-white d-inline-block mb-0">{{ $model_object['model'] }}</h6>
    <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
        <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
            <li class="breadcrumb-item"><a href="/"><i class="fas fa-home"></i></a></li>
            <li class="breadcrumb-item"><a href="/home">Dashboard </a></li>
            @foreach ($model_object['breadcrumb'] as $key => $link)
                <li class="breadcrumb-item"><a href="{{ $link }}"> {{ $key }} </a></li>
            @endforeach
            <li class="breadcrumb-item active" aria-current="page">View</li>
        </ol>
    </nav>
@endsection

@section('header_right')
    <a href="{{ url($model_object['model_path']) }}" class="btn btn-sm btn-neutral text-default">
        <span class="btn-inner--icon"><i class="ni ni-bold-left"></i></span>
        Back to Data List
    </a>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-10">
            <div class="card-wrapper">
                <div class="card">
                    <div class="card-header">
                        <h3 class="mb-0 font-weight-900">
                            View Existing {{ $model_object['model'] }}
                        </h3>
                    </div>

                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <img class="img-fluid" src="{{ url($detail_collection->image) }}">
                            </div>

                            <div class="col-md-6">
                                <label class="form-control-label" for="textarea-title">Title</label>
                                <textarea class="form-control" id="textarea-title" name="title"
                                          rows="3" readonly>{{ $detail_collection->title }}</textarea>
                                <hr>

                                <label class="form-control-label" for="textarea-body">Body</label>
                                <textarea class="form-control" id="textarea-body" name="body"
                                          rows="3" readonly>{{ $detail_collection->body }}</textarea>

                                <hr>

                                <div class="form-group">
                                    <label class="form-control-label" for="form-updated_at"> Updated At </label>
                                    <input type="text" class="form-control"
                                           id="form-updated_at" name="updated_at"
                                           value="{{ $detail_collection->updated_at }}"
                                           readonly>
                                </div>

                                <div class="form-group">
                                    <label class="form-control-label" for="form-created_at"> Created At </label>
                                    <input type="text" class="form-control"
                                           id="form-created_at" name="created_at"
                                           value="{{ $detail_collection->created_at }}"
                                           readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection