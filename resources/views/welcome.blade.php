<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Pyxis</title>
    <link rel="short icon" href="assets/logo.png">
    <link rel="stylesheet" href="assets/style.css">
</head>
<body>
<header>
    <div class="container">
        <img src="assets/header.svg" alt="" height="50px;">
    </div>
</header>

<main id="landing">
    <div id="landing-hero">
        <div id="first" class="flex center">
            <div id="hero" class="container flex center">
                <div id="hero-images" class="row">
                    <img src="assets/hero%20copy.png" alt="Hero image">
                </div>
                <div id="about" class="row">
                    <h6 style="color: #555;">ABOUT THE APP</h6>
                    <h1>Meet your new hyper local social app.</h1>
                    <p style="padding: 25px 0px;">Super App that bridges the information gap and tries to bring all city
                        services in one app. </p>
                    <a href="#" id="download">
                        <img src="assets/google-play-badge.svg" alt="">
                    </a>
                </div>
            </div>
        </div>
    </div>
    <!-- Border-->
    <div class="curvy-boi"></div>

    <div id="second">
        <h1>Super app which spirals user and revenue growth.</h1>
        <div id="badges" class="container flex just-center">
            <!-- News -->
            <div id="news" class="badge">
                <img src="assets/news.svg" alt="">
                <h3>Keep up with the local news.</h3>
            </div>
            <!-- News -->
            <div id="book" class="badge">
                <img src="assets/book.svg" alt="">
                <h3>Easily book reservations</h3>
            </div>
            <!-- News -->
            <div id="social" class="badge">
                <img src="assets/social.svg" alt="">
                <h3>Catch up with your peers</h3>
            </div>
        </div>
    </div>
</main>
<footer style="padding: 50px; text-align: center; font-family: 'Roboto', sans-serif; font-weight: 100; box-shadow: 1px -10px 10px rgba(0,0,0,0.1);">
    <p>Pyxis &copy; 2019. All Rights Reserved. </p>
</footer>
</body>
</html>