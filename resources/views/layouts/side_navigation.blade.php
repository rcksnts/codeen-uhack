<nav class="sidenav navbar navbar-vertical fixed-left navbar-expand-xs navbar-light bg-gradient-white"
     id="sidenav-main">
    <div class="scrollbar-inner">
        <div class="sidenav-header d-flex align-items-center">
            <a class="navbar-brand" href="/">
                <img src="{{ asset('assets/icon.png') }}"
                     class="navbar-brand-img">
            </a>
            <div class="ml-auto">
                <div class="sidenav-toggler d-none d-xl-block" data-action="sidenav-unpin" data-target="#sidenav-main">
                    <div class="sidenav-toggler-inner">
                        <i class="sidenav-toggler-line"></i>
                        <i class="sidenav-toggler-line"></i>
                        <i class="sidenav-toggler-line"></i>
                    </div>
                </div>
            </div>
        </div>
        <div class="navbar-inner">
            <div class="collapse navbar-collapse" id="sidenav-collapse-main">
                <ul class="navbar-nav">
                    <li class="nav-item @if(strpos($page_setting['current_path'], 'home') !== false)) active @endif">
                        <a class="nav-link @if(strpos($page_setting['current_path'], 'home') !== false)) active @endif"
                           href="/home" id="dashboard">
                            <i class="ni ni-shop"></i>
                            <span class="nav-link-text"> Dashboard </span>
                        </a>
                    </li>

                    <li class="nav-item @if(strpos($page_setting['current_path'], 'user') !== false)) active @endif">
                        <a class="nav-link @if(strpos($page_setting['current_path'], 'user') !== false)) active @endif"
                           href="/user" id="users">
                            <i class="fas fa-user-friends"></i>
                            <span class="nav-link-text"> Users </span>
                        </a>
                    </li>
                </ul>

                <hr class="my-3">

                <ul class="navbar-nav">
                    <li class="nav-item @if(strpos($page_setting['current_path'], 'service') !== false) active @endif">
                        <a class="nav-link @if(strpos($page_setting['current_path'], 'service') !== false)) active @endif"
                           href="/service" id="services">
                            <i class="fas fa-briefcase"></i>
                            <span class="nav-link-text"> Services </span>
                        </a>
                    </li>

                    <li class="nav-item @if(strpos($page_setting['current_path'], 'calendar') !== false) active @endif">
                        <a class="nav-link @if(strpos($page_setting['current_path'], 'calendar') !== false)) active @endif"
                           href="/calendar" id="calendar">
                            <i class="far fa-calendar-alt"></i>
                            <span class="nav-link-text"> Calendar </span>
                        </a>
                    </li>
                </ul>

                <hr class="my-3">

                <ul class="navbar-nav">
                    <li class="nav-item @if(strpos($page_setting['current_path'], 'article') !== false) active @endif">
                        <a class="nav-link @if(strpos($page_setting['current_path'], 'article') !== false)) active @endif"
                           href="/article" id="articles">
                            <i class="far fa-newspaper"></i>
                            <span class="nav-link-text"> News Articles </span>
                        </a>
                    </li>

                    <li class="nav-item @if(strpos($page_setting['current_path'], 'city_development') !== false) active @endif">
                        <a class="nav-link @if(strpos($page_setting['current_path'], 'city_development') !== false)) active @endif"
                           href="/city_development" id="city_developments">
                            <i class="fas fa-city"></i>
                            <span class="nav-link-text"> City Developments </span>
                        </a>
                    </li>
                </ul>

                <hr class="my-3">

                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a href="{{ route('logout') }}" class="nav-link"
                           onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                            <i class="fas fa-sign-out-alt"></i>
                            <span class="nav-link-text">Logout</span>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>