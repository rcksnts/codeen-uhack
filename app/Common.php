<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Str;

class Common extends Model
{
    public static function getModelObject($model, $model_path, $header, $param, $alias = null)
    {
        $model_object = [];
        $model_object['model'] = $model;
        $model_object['model_path'] = $model_path;
        $model_object['header_list'] = $header;

        if ($alias == null) {
            $model_object['breadcrumb'] = array(Str::singular($model) => $model_path);
        } else {
            $model_object['breadcrumb'] = array(Str::singular($alias) => $model_path);
        }

        switch ($param['action']) {
            case 'index':
                $model_object['action'] = '';
                $model_object['form_action'] = $model_path . '/create';
                break;
            case 'create':
                $model_object['action'] = 'create';
                $model_object['form_action'] = $model_path;
                break;
            case 'edit':
                $model_object['action'] = 'edit';
                $model_object['form_action'] = $model_path . '/' . $param['detail_id'] . '/update';
                break;
            case 'show':
                $model_object['action'] = 'show';
                break;
            default:
                break;
        }

        return $model_object;
    }

    public static function generateActionColumn($id, $path)
    {
        $view_route = URL::to($path) . '/' . $id;
        $edit_route = URL::to($path) . '/' . $id . '/edit';

        $action_html = '';
        $action_html .= '<a href="' . $view_route . '" class="table-action"><i class="fas fa-eye"></i></a>';
        if ($path != '/user') {
            $action_html .= '<a href="' . $edit_route . '" class="table-action"><i class="fas fa-pen"></i></a>';
        }

        return $action_html;
    }
}
