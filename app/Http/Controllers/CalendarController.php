<?php

namespace App\Http\Controllers;

use App\Service;
use App\UserBooking;
use App\UserDetail;

class CalendarController extends Controller
{
    public function index()
    {
        $model_object['index'] = '/calendar';
        return view('calendar', compact('model_object'));
    }

    public function retrieveDateList()
    {
        $dates = collect();

        $user_bookings = UserBooking::all();

        foreach ($user_bookings as $user_booking) {
            $user_booking->service = Service::findOrFail($user_booking->service_id)->name;
            $customer = UserDetail::where('user_id', $user_booking->user_id)->first();
            $user_booking->user = $customer->first_name . ' ' . $customer->last_name;

            $user_booking->title = $user_booking->service;
            $user_booking->className = 'bg-info';
            $user_booking->start = $user_booking->reserved_schedule;
            $user_booking->end = date("Y-m-d H:i:s", strtotime('+1 hours', strtotime($user_booking->date)));
            $user_booking->allDay = 'false';
            $user_booking->type = 'Booking';

            $dates->push($user_booking);
        }

        $data = array();
        $data['events'] = $dates;
        $data['message'] = 'Calendar fetched successfully';

        $response['data'] = $data;

        return response()->json($response);
    }
}
