<?php

namespace App\Http\Controllers;

use App\Common;
use App\Service;
use App\ServiceImage;
use App\ServiceType;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Str;
use Yajra\DataTables\Facades\DataTables;

class ServiceController extends Controller
{
    public static $module_name = 'Service';
    public static $module_path = '/service';
    public static $header = ['name', 'service type'];

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $param['action'] = 'index';
        $model_object = Common::getModelObject(self::$module_name, self::$module_path, self::$header, $param);

        return view('services.index', compact('model_object'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $param['action'] = 'create';
        $model_object = Common::getModelObject(self::$module_name, self::$module_path, self::$header, $param);

        $service_types = ServiceType::all();

        return view('services.service_form', compact('model_object', 'service_types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $form_input = $request->all();
        $detail_collection = new Service($form_input);
        $detail_collection->city_id = '1';
        $detail_collection->service_type_id = $request->service_type;

        $image = $request->file('image');
        $filename = preg_replace('/\s+/', '', Str::uuid() . '.' . $image->getClientOriginalExtension());
        $directory = preg_replace('/\s+/', '', '/codeen/services/' . $filename);
        $image->move(public_path() . '/codeen/services/', $filename);

        if ($detail_collection->save()) {
            $service_image = new ServiceImage();
            $service_image->service_id = $detail_collection->id;
            $service_image->image_link = $directory;
            $service_image->save();

            $status = 'success';
            $response = self::$module_name . ' has been created successfully.';
        } else {
            $status = 'error';
            $response = 'Failed to add ' . self::$module_name . '. Please try again.';
        }

        return redirect(self::$module_path)->with($status, $response);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        $detail_collection = Service::findOrFail($id);
        $detail_collection->service_type = $detail_collection->service_type->name;
        if ($detail_collection->service_image->image_link) {
//            $detail_collection->image = Storage::disk('s3')->url($detail_collection->article_image->image_link);
            $detail_collection->image = $detail_collection->service_image->image_link;
        }

        $param['action'] = 'show';
        $model_object = Common::getModelObject(self::$module_name, self::$module_path, self::$header, $param);

        return view('services.service_details', compact('model_object', 'detail_collection'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $detail_collection = Service::findOrFail($id);
        $service_types = ServiceType::all();

        $param['action'] = 'edit';
        $param['detail_id'] = $id;
        $model_object = Common::getModelObject(self::$module_name, self::$module_path, self::$header, $param);

        if ($detail_collection->service_image->image_link) {
//            $detail_collection->image = Storage::disk('s3')->url($detail_collection->article_image->image_link);
            $detail_collection->image = $detail_collection->service_image->image_link;
        }

        return view('services.service_form', compact('model_object', 'detail_collection', 'service_types'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $detail_collection = Service::findOrFail($id);
        $detail_collection->name = $request->name;
        $detail_collection->description = $request->description;
        $detail_collection->city_id = '1';
        $detail_collection->service_type_id = $request->service_type;

        if ($detail_collection->save()) {
            $status = 'success';
            $response = self::$module_name . ' has been updated successfully.';
        } else {
            $status = 'error';
            $response = 'Failed to update ' . self::$module_name . '. Please try again.';
        }

        return redirect(self::$module_path)->with($status, $response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    public function retrieveList()
    {
        $detail_collection = Service::query();

        return DataTables::eloquent($detail_collection)
            ->addColumn('action_column', function ($list) {
                $action_html = Common::generateActionColumn($list->id, self::$module_path);

                return $action_html;
            })
            ->addColumn('service_type', function ($list) {
                $service_type = ServiceType::findOrFail($list->service_type_id)->name;

                return $service_type;
            })
            ->rawColumns(['action_column'])
            ->toJson();
    }
}
