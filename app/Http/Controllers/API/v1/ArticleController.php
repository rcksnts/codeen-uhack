<?php

namespace App\Http\Controllers\API\v1;

use App\Article;
use App\Http\Controllers\Controller;

class ArticleController extends Controller
{
    private $limit = 15;

    public function getArticleHeadline()
    {
        $response = array();

        $article = Article::with('article_image')
            ->where('is_highlighted', true)->first();

        $data = array();
        $data['article'] = $article;

        $response['data'] = $data;
        $response['message'] = 'Article headline fetched';

        return response()->json($response);
    }

    public function getInitialArticles()
    {
        $response = array();

        $articles = Article::with('article_image')
            ->offset(0)
            ->limit($this->limit)
            ->get();

        $data = array();
        $data['articles'] = $articles;
        $data['message'] = 'Initial Articles fetched successfully';

        $response['data'] = $data;

        return response()->json($response);
    }

    public function getInitialPopularArticles()
    {
        $response = array();

        $articles = Article::with('article_image')
            ->orderBy('view_count', 'desc')
            ->offset(0)
            ->limit($this->limit)
            ->get();

        $data = array();
        $data['articles'] = $articles;
        $data['message'] = 'Initial Popular Articles fetched successfully';

        $response['data'] = $data;

        return response()->json($response);
    }

    public function getNewerArticles($article_id)
    {
        $response = array();

        $article_reference = Article::where('id', $article_id)->first();
        $articles = Article::with('article_image')
            ->where('created_at', '>=', $article_reference->created_at)
            ->where('id', '!=', $article_reference->id)
            ->orderBy('created_at', 'desc')
            ->get();

        $data = array();
        $data['articles'] = $articles;
        $data['message'] = 'Newer articles fetched successfully';

        $response['data'] = $data;

        return response()->json($response);
    }

    public function getNewerPopularArticles($article_id)
    {
        $response = array();

        $article_reference = Article::where('id', $article_id)->first();
        $articles = Article::with('article_image')
            ->where('created_at', '>=', $article_reference->created_at)
            ->where('id', '!=', $article_reference->id)
            ->orderBy('created_at', 'desc')
            ->orderBy('view_count', 'desc')
            ->get();

        $data = array();
        $data['articles'] = $articles;
        $data['message'] = 'Newer popular articles fetched successfully';

        $response['data'] = $data;

        return response()->json($response);
    }

    public function getOlderArticles($article_id)
    {
        $response = array();

        $article_reference = Article::where('id', $article_id)->first();
        $articles = Article::with('article_image')
            ->where('created_at', '<=', $article_reference->created_at)
            ->where('id', '!=', $article_reference->id)
            ->orderBy('created_at', 'desc')
            ->get();

        $data = array();
        $data['articles'] = $articles;
        $data['message'] = 'Older articles fetched successfully';

        $response['data'] = $data;

        return response()->json($response);
    }

    public function getOlderPopularArticles($article_id)
    {
        $response = array();

        $article_reference = Article::where('id', $article_id)->first();
        $articles = Article::with('article_image')
            ->where('created_at', '<=', $article_reference->created_at)
            ->where('id', '!=', $article_reference->id)
            ->orderBy('created_at', 'desc')
            ->orderBy('view_count', 'desc')
            ->get();

        $data = array();
        $data['articles'] = $articles;
        $data['message'] = 'Older popular articles fetched successfully';

        $response['data'] = $data;

        return response()->json($response);
    }
}
