<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Controllers\Controller;
use App\UserBooking;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class UserBookingController extends Controller
{
    public function bookSchedule(Request $request)
    {
        Log::debug($request);

        $response = array();

        $user_id = $request->user_id;
        $service_id = $request->service_id;
        $reserved_schedule = $request->reserved_schedule;

        $user_booking = new UserBooking();
        $user_booking->user_id = $user_id;
        $user_booking->service_id = $service_id;
        $user_booking->reserved_schedule = $reserved_schedule;
        $user_booking->save();

        $data = array();
        $data['user_booking'] = $user_booking;

        $response['data'] = $data;
        $response['message'] = 'User booking successful';

        return response()->json($response);
    }
}
