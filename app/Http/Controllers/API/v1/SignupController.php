<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Controllers\Controller;
use App\User;
use App\UserDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class SignupController extends Controller
{
    public function signup(Request $request)
    {
        $response = array();

        $email = $request->email;
        $password = $request->password;
        $user_type_id = $request->user_type_id;

        $supplier_id = $request->supplier_id;
        $company_id = $request->company_id;

        $first_name = $request->first_name;
        $last_name = $request->last_name;

        try {
            DB::transaction(function () use ($email, $password, $first_name, $last_name, $user_type_id, $supplier_id, $company_id) {
                $user = new User();
                $user->email = $email;
                $user->password = Hash::make($password);
                $user->user_type_id = $user_type_id;
                if ($supplier_id != null) {
                    $user->supplier_id = $supplier_id;
                } else if ($company_id != null) {
                    $user->company_id = $company_id;
                } else {
                    throw new \Exception();
                }
                $user->save();

                $user_detail = new UserDetail();
                $user_detail->user_id = $user->id;
                $user_detail->first_name = $first_name;
                $user_detail->last_name = $last_name;
                $user_detail->save();

                $data = array();
                $data['user'] = $user;
                $data['user']['user_detail'] = $user_detail;

                $response['data'] = $data;
                $response['message'] = 'Signup successful';
            });
        } catch (\Exception $exception) {
            Log::error($exception->getMessage());
            Log::error($exception->getTraceAsString());

            $response['message'] = 'Signup failed';
        }

        return response()->json($response);
    }
}
