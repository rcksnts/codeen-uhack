<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Controllers\Controller;
use App\User;

class UserProfileController extends Controller
{
    public function getUserProfile()
    {
        $response = array();

        $user_id = 2;
        $user = User::with('user_detail', 'user_image')
            ->where('id', $user_id)->first();

        $data = array();
        $data['user'] = $user;

        $response['data'] = $data;
        $data['message'] = 'User profile fetched successfully';

        return response()->json($response);
    }
}
