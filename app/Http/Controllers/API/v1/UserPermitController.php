<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Controllers\Controller;
use App\UserPermit;

class UserPermitController extends Controller
{
    public function getUserPermits()
    {
        $response = array();

        $user_id = 2;
        $user_permits = UserPermit::with('permit_type')
            ->where('user_id', $user_id)->get();

        $data = array();
        $data['user_permits'] = $user_permits;

        $response['data'] = $data;
        $response['message'] = 'User permits successfully fetched';

        return response()->json($response);
    }
}
