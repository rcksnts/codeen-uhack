<?php

namespace App\Http\Controllers\API\v1;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    public function login(Request $request)
    {
        $response = array();

        $email = $request->email;
        $password = $request->password;

        $user = User::where('email', $email)->first();
        if ($user != null) {
            if (Hash::check($password, $user->password)) {
                $data = array();
                $data['user'] = $user;

                $response['data'] = $data;
                $response['message'] = 'Login successful';
            } else {
                $response['message'] = 'Login failed. Invalid email/password.';
            }
        } else {
            $response['message'] = 'Login failed. The email you provided is not associated with any account.';
        }

        return response()->json($response);
    }
}
