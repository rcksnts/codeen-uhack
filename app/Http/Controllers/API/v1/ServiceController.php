<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Controllers\Controller;
use App\Service;

class ServiceController extends Controller
{
    public function getServices()
    {
        $response = array();

        $services = Service::with('service_type', 'service_image')->get();

        $data = array();
        $data['services'] = $services;

        $response['data'] = $data;

        return response()->json($response);
    }
}
