<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Controllers\Controller;
use App\Post;
use App\PostImage;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class PostController extends Controller
{
    private $limit = 100;

    public function createPost(Request $request)
    {
        $response = array();

        $user_id = $request->user_id;
        $title = $request->title;
        $body = $request->body;
        $image = $request->image;

        $image = $request->image;  // your base64 encoded
        $image = str_replace('data:image/png;base64,', '', $image);
        $image = str_replace(' ', '+', $image);
        $imageName = str_random(10) . '.' . 'png';
        $imageDir = '/post_images/' . $imageName;
        File::put(public_path() . $imageDir, base64_decode($image));

        $post = new Post();
        $post->user_id = $user_id;
        $post->title = $title;
        $post->body = $body;
        $post->city_id = 1;
        $post->save();

        $post_image = new PostImage();
        $post_image->post_id = $post->id;
        $post_image->image_link = $imageDir;
        $post_image->save();

        $user = User::with('user_detail', 'user_image')
            ->where('id', $user_id)->first();

        $data = array();
        $data['post'] = $post;
        $data['post']['post_image'] = $post_image;
        $data['post']['user'] = $user;

        $response['data'] = $data;
        $response['message'] = 'Post created successfully';

        return $response;
    }

    public function getInitialPosts()
    {
        $response = array();

        $posts = Post::with('post_image', 'user.user_detail', 'user.user_image')
            ->offset(0)
            ->limit($this->limit)
            ->get();

        $data = array();
        $data['posts'] = $posts;
        $data['message'] = 'Initial Posts fetched successfully';

        $response['data'] = $data;

        return response()->json($response);
    }

    public function getNewerPosts($post_id)
    {
        $response = array();

        $post_reference = Post::where('id', $post_id)->first();
        $posts = Post::with('post_image', 'user', 'user.user_detail', 'user.user_image')
            ->where('created_at', '>=', $post_reference->created_at)
            ->where('id', '!=', $post_reference->id)
            ->orderBy('created_at', 'desc')
            ->get();

        $data = array();
        $data['posts'] = $posts;
        $data['message'] = 'Newer posts fetched successfully';

        $response['data'] = $data;

        return response()->json($response);
    }

    public function getOlderPosts($post_id)
    {
        $response = array();

        $post_reference = Post::where('id', $post_id)->first();
        $posts = Post::with('post_image', 'user.user_detail', 'user.user_image')
            ->where('created_at', '<=', $post_reference->created_at)
            ->where('id', '!=', $post_reference->id)
            ->orderBy('created_at', 'desc')
            ->get();

        $data = array();
        $data['posts'] = $posts;
        $data['message'] = 'Older posts fetched successfully';

        $response['data'] = $data;

        return response()->json($response);
    }
}
