<?php

namespace App\Http\Controllers;

use App\CityDevelopment;
use App\Common;
use App\Poc;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Yajra\DataTables\Facades\DataTables;

class CityDevelopmentController extends Controller
{
    public static $module_name = 'City Development';
    public static $module_path = '/city_development';
    public static $header = ['name', 'status'];

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $param['action'] = 'index';
        $model_object = Common::getModelObject(self::$module_name, self::$module_path, self::$header, $param);

        return view('city_developments.index', compact('model_object'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $param['action'] = 'create';
        $model_object = Common::getModelObject(self::$module_name, self::$module_path, self::$header, $param);

        return view('city_developments.city_development_form', compact('model_object'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $form_input = $request->all();
        $detail_collection = new CityDevelopment($form_input);
        $detail_collection->city_id = '1';
        $detail_collection->status = 'CTDEV01';

        if ($detail_collection->save()) {
            $poc = new Poc();
            $poc->percentage = 0.00;
            $poc->city_development_id = $detail_collection->id;
            $poc->milestone = $detail_collection->status;
            $poc->description = null;
            $poc->save();

            $status = 'success';
            $response = self::$module_name . ' has been created successfully.';
        } else {
            $status = 'error';
            $response = 'Failed to add ' . self::$module_name . '. Please try again.';
        }

        return redirect(self::$module_path)->with($status, $response);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        $detail_collection = CityDevelopment::findOrFail($id);
        $poc = Poc::where('city_development_id', $detail_collection->id)
            ->where('milestone', $detail_collection->status)->first();

        $detail_collection->percentage = $poc->percentage;

        $param['action'] = 'show';
        $model_object = Common::getModelObject(self::$module_name, self::$module_path, self::$header, $param);

        return view('city_developments.city_development_details', compact('model_object', 'detail_collection'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $detail_collection = CityDevelopment::findOrFail($id);
        $poc = Poc::where('city_development_id', $detail_collection->id)
            ->where('milestone', $detail_collection->status)->orderBy('created_at', 'desc')->first();

        $detail_collection->percentage = $poc->percentage;

        $param['action'] = 'edit';
        $param['detail_id'] = $id;
        $model_object = Common::getModelObject(self::$module_name, self::$module_path, self::$header, $param);

        return view('city_developments.city_development_form', compact('model_object', 'detail_collection'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $detail_collection = CityDevelopment::findOrFail($id);
        $detail_collection->name = $request->name;
        $detail_collection->description = $request->description;
        $detail_collection->status = $request->stage;

        if ($detail_collection->save()) {
            $poc = new Poc();
            $poc->percentage = $request->percentage;
            $poc->city_development_id = $detail_collection->id;
            $poc->milestone = $detail_collection->status;
            $poc->description = null;
            $poc->save();

            $status = 'success';
            $response = self::$module_name . ' has been updated successfully.';
        } else {
            $status = 'error';
            $response = 'Failed to update ' . self::$module_name . '. Please try again.';
        }

        return redirect(self::$module_path)->with($status, $response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    public function retrieveList()
    {
        $detail_collection = CityDevelopment::query();

        return DataTables::eloquent($detail_collection)
            ->addColumn('action_column', function ($list) {
                $action_html = Common::generateActionColumn($list->id, self::$module_path);

                return $action_html;
            })
            ->rawColumns(['action_column'])
            ->toJson();
    }
}
