<?php

namespace App\Http\Controllers;

use App\CityDevelopment;
use App\Common;
use App\User;
use App\UserDetail;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Yajra\DataTables\Facades\DataTables;

class UserController extends Controller
{
    public static $module_name = 'User';
    public static $module_path = '/user';
    public static $header = ['email', 'first name', 'last name'];

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $param['action'] = 'index';
        $model_object = Common::getModelObject(self::$module_name, self::$module_path, self::$header, $param);

        return view('users.index', compact('model_object'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        $detail_collection = User::findOrFail($id);

        $param['action'] = 'show';
        $model_object = Common::getModelObject(self::$module_name, self::$module_path, self::$header, $param);

        return view('users.user_details', compact('model_object', 'detail_collection'));
    }

    public function retrieveList()
    {
        $detail_collection = User::query();

        return DataTables::eloquent($detail_collection)
            ->addColumn('action_column', function ($list) {
                $action_html = Common::generateActionColumn($list->id, self::$module_path);

                return $action_html;
            })
            ->addColumn('first_name', function ($list) {
                $first_name = UserDetail::where('user_id', $list->id)->first()->first_name;

                return $first_name;
            })
            ->addColumn('last_name', function ($list) {
                $last_name = UserDetail::where('user_id', $list->id)->first()->last_name;

                return $last_name;
            })
            ->rawColumns(['action_column'])
            ->toJson();
    }
}
