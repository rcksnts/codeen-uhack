<?php

namespace App\Http\Controllers;

use App\Article;
use App\ArticleImage;
use App\Common;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Yajra\DataTables\Facades\DataTables;

class ArticleController extends Controller
{
    public static $module_name = 'Article';
    public static $module_path = '/article';
    public static $header = ['title', 'body', 'view_count'];

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $param['action'] = 'index';
        $model_object = Common::getModelObject(self::$module_name, self::$module_path, self::$header, $param);

        return view('articles.index', compact('model_object'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $param['action'] = 'create';
        $model_object = Common::getModelObject(self::$module_name, self::$module_path, self::$header, $param);

        return view('articles.articles_form', compact('model_object'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $form_input = $request->all();
        $detail_collection = new Article($form_input);
        $detail_collection->city_id = '1';
        $detail_collection->view_count = '0';

        $image = $request->file('image');
        $filename = preg_replace('/\s+/', '', Str::uuid() . '.' . $image->getClientOriginalExtension());
        $directory = preg_replace('/\s+/', '', '/codeen/articles/' . $filename);
        $image->move(public_path() . '/codeen/articles/', $filename);

        if ($detail_collection->save()) {
            $article_image = new ArticleImage();
            $article_image->article_id = $detail_collection->id;
            $article_image->image_link = $directory;
            $article_image->save();

            $status = 'success';
            $response = self::$module_name . ' has been created successfully.';
        } else {
            $status = 'error';
            $response = 'Failed to add ' . self::$module_name . '. Please try again.';
        }

        return redirect(self::$module_path)->with($status, $response);
    }

//    public static function upload($file, $file_directory)
//    {
//        $is_file_uploaded = false;
//        try {
//            $is_file_uploaded = Storage::disk('s3')->put($file_directory, file_get_contents($file), 'public');
//        } catch (Exception $exception) {
//            Log::error("Failed to upload file to S3: " . $file_directory);
//            Log::error($exception->getTraceAsString());
//        }
//
//        return $is_file_uploaded;
//    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        $detail_collection = Article::findOrFail($id);
        if ($detail_collection->article_image->image_link) {
//            $detail_collection->image = Storage::disk('s3')->url($detail_collection->article_image->image_link);
            $detail_collection->image = $detail_collection->article_image->image_link;
        }

        $param['action'] = 'show';
        $model_object = Common::getModelObject(self::$module_name, self::$module_path, self::$header, $param);

        return view('articles.articles_details', compact('model_object', 'detail_collection'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $detail_collection = Article::findOrFail($id);

        $param['action'] = 'edit';
        $param['detail_id'] = $id;
        $model_object = Common::getModelObject(self::$module_name, self::$module_path, self::$header, $param);

        if ($detail_collection->article_image->image_link) {
//            $detail_collection->image = Storage::disk('s3')->url($detail_collection->article_image->image_link);
            $detail_collection->image = $detail_collection->article_image->image_link;
        }

        return view('articles.articles_form', compact('model_object', 'detail_collection'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $detail_collection = Article::findOrFail($id);
        $detail_collection->title = $request->title;
        $detail_collection->body = $request->body;

        if ($detail_collection->save()) {
            $status = 'success';
            $response = self::$module_name . ' has been updated successfully.';
        } else {
            $status = 'error';
            $response = 'Failed to update ' . self::$module_name . '. Please try again.';
        }

        return redirect(self::$module_path)->with($status, $response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    public function retrieveList()
    {
        $detail_collection = Article::query();

        return DataTables::eloquent($detail_collection)
            ->addColumn('action_column', function ($list) {
                $action_html = Common::generateActionColumn($list->id, self::$module_path);

                return $action_html;
            })
            ->rawColumns(['action_column'])
            ->toJson();
    }
}
