<?php

namespace App\Http\ViewComposers;

use App\UserDetail;
use App\UserType;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\View\View;

class AdminNavigationComposer
{
    /**
     * Bind data to the view.
     *
     * @param View $view
     * @return void
     */
    public function compose(View $view)
    {
        $current_user = Auth::user();
        $page_setting = array();
        $page_setting['current_path'] = Route::currentRouteName();

        if ($current_user) {
            $user_detail = UserDetail::where('user_id', $current_user->id)->first();
            $user_type = UserType::where('id', $current_user->user_type_id)->first();

            $page_setting['current_user'] = $user_detail->first_name . ' ' . $user_detail->last_name;
            $page_setting['user_type'] = $user_type->name;
        }

        $view->with('page_setting', $page_setting);
    }
}