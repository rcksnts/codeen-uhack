<?php

use App\ServiceType;
use Illuminate\Database\Seeder;

class ServiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $service_type = new ServiceType();
        $service_type->name = 'Restaurants';
        $service_type->city_id = '1';
        $service_type->save();

        $service_type = new ServiceType();
        $service_type->name = 'Spa, Beauty & Wellness';
        $service_type->city_id = '1';
        $service_type->save();

        $service_type = new ServiceType();
        $service_type->name = 'Shopping and Products';
        $service_type->city_id = '1';
        $service_type->save();

        $service_type = new ServiceType();
        $service_type->name = 'Exciting Activities';
        $service_type->city_id = '1';
        $service_type->save();
    }
}
