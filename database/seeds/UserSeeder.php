<?php

use App\City;
use App\User;
use App\UserDetail;
use App\UserImage;
use App\UserType;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $city = new City();
        $city->name = 'Sample City';
        $city->save();

        $user_type = new UserType();
        $user_type->name = 'Administrator';
        $user_type->save();

        $user_type = new UserType();
        $user_type->name = 'Customer';
        $user_type->save();

        $user = new User();
        $user->email = 'admin@codeen.com';
        $user->password = bcrypt('admin');
        $user->user_type_id = '1';
        $user->save();

        $user_detail = new UserDetail();
        $user_detail->user_id = $user->id;
        $user_detail->first_name = 'Codeen';
        $user_detail->last_name = 'Administrator';
        $user_detail->save();

        $user = new User();
        $user->email = 'user@codeen.com';
        $user->password = bcrypt('password');
        $user->user_type_id = '2';
        $user->save();

        $user_detail = new UserDetail();
        $user_detail->user_id = $user->id;
        $user_detail->first_name = 'Codeen';
        $user_detail->last_name = 'User';
        $user_detail->save();

        $user_image = new UserImage();
        $user_image->user_id = $user->id;
        $user_image->image_link = '/assets/user_icon.jpg';
        $user_image->save();

        $users = factory(App\User::class, 30)->create();

        foreach ($users as $user) {
            $user_detail = factory(App\UserDetail::class)->create([
                'user_id' => $user->id,
            ]);

            $user_image = new UserImage();
            $user_image->user_id = $user->id;
            $user_image->image_link = '/assets/user_icon.jpg';
            $user_image->save();
        }
    }
}
