<?php

use App\PermitType;
use Illuminate\Database\Seeder;

class PermitTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permit_type = new PermitType();
        $permit_type->name = 'Residence Permit';
        $permit_type->save();
    }
}
